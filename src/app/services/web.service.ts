import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';

const httpOption = {
  headers: new HttpHeaders({'Content-type':"application/json"})
};

@Injectable({
  providedIn: 'root'
})
export class WebService {

  constructor(private http:HttpClient) { }

  registerNewUser(userData){
    return this.http.get('/server/api/v2/user');
  }
}
