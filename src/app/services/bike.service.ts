import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';

const httpOption = {
  headers: new HttpHeaders({'Content-type':"application/json"})
};

@Injectable({
  providedIn: 'root'
})
export class BikeService {

  constructor(private http:HttpClient) { }

  getBikeData(){
    return this.http.get('/server/api/v1/bikes');
  }

  gitBike(id: number){
    return this.http.get('/server/api/v1/bikes/'+ id);
  }

  bikeRegistration(bike){
    let body = JSON.stringify(bike);
    return this.http.post('/server/api/v1/bikes', body, httpOption);
  }

  registerNewUser(userData){
    let body = JSON.stringify(userData);
    return this.http.post('/server/api/v2/user', body, httpOption);
  }
}
