 import { Component, OnInit } from '@angular/core';
 import { BikeService } from '../../services/bike.service';
 import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  public user = {
    "name":"",
    "emilId":"",
    "contactNo":"",
    "password":"",
    "confirmPassword":""
  };

  public validateData = {
    "status":true,
    "msg":""
  }
  public alertMsg:string;
  constructor(private bikeService:BikeService, private route:ActivatedRoute) { }

  ngOnInit() {
  }

  public Register():void{
  
    this.validateData = this.ValidateSignUpForm(this.user)
    if(this.validateData.status){
        
        this.bikeService.getBikeData().subscribe(
          data =>{

          }, error => console.log(error)
        )
    } else {
      this.alertMsg = this.validateData.msg;
    }
  }

  public ValidateSignUpForm(user){
    return this.validateData;
  }

}
