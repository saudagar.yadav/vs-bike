import { Component, OnInit } from '@angular/core';
import { BikeService } from '../../services/bike.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  models: string[] = [
    'Pluser',
    'Honda',
    'Royal'
  ];
  bikeform : FormGroup;
  validMessage : string= "";
  constructor(private bikeService:BikeService) { }

  ngOnInit() {
    this.bikeform = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      model: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      purchasePrice: new FormControl('', Validators.required),
      purchaseDate: new FormControl('', Validators.required),
      serialNumber: new FormControl('', Validators.required)
    });
  }

  public submitRegistration():void{
    if(this.bikeform.valid){
      this.validMessage = 'Hogaya registration successfully. Thank you ';
      this.bikeService.bikeRegistration(this.bikeform.value).subscribe(
        data =>{
          this.bikeform.reset;
          return true;
        }, error => console.log(error)
      )
    }else{
      this.validMessage = "*All fields are mandatory";
    }
  }

}
