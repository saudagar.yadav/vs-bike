import { Component, OnInit } from '@angular/core';
import { BikeService } from '../../services/bike.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  validMessage : string= "";
  constructor(private bikeService:BikeService) { }

  ngOnInit() {
    this.bikeService.getBikeData();
  }

  public submitRegistration():void{
  }

}

