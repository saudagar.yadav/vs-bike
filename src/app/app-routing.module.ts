import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { HomeComponent } from './components/home/home.component';
import { ViewRegistrationComponent } from './components/view-registration/view-registration.component';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';

const routes: Routes = [

  {
    path:'',
    component:LoginComponent
  },{
    path:'login',
    component:LoginComponent
  },{
    path:'signUp',
    component:SignUpComponent
  },{
    path:'bike',
    component:HomeComponent
  },{
    path:'admin/view/:id',
    component:ViewRegistrationComponent
  },{
    path:'admin',
    component:AdminComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
